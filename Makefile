.PHONY: all

all:
	@mkdir -p build/
	@echo "Linux/ARM..."
	@GOOS=linux GOARCH=arm go build -o build/tplink-hsxxx-mqtt-linux-arm .
	@zip -D -m build/tplink-hsxxx-mqtt-linux-arm.zip build/tplink-hsxxx-mqtt-linux-arm
	@echo "Linux/AMD64..."
	@GOOS=linux GOARCH=amd64 go build -o build/tplink-hsxxx-mqtt-linux-amd64 .
	@zip -D -m build/tplink-hsxxx-mqtt-linux-amd64.zip build/tplink-hsxxx-mqtt-linux-amd64
	@echo "Windows/AMD64..."
	@GOOS=windows GOARCH=amd64 go build -o build/tplink-hsxxx-mqtt-windows-amd64 .
	@zip -D -m build/tplink-hsxxx-mqtt-windows-amd64.zip build/tplink-hsxxx-mqtt-windows-amd64
