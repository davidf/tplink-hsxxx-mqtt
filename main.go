package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"math/rand"
	"time"

	"github.com/yosssi/gmq/mqtt"
	"github.com/yosssi/gmq/mqtt/client"
	"gitlab.com/davidf/tplink-wifi-switch-api/hsplug"
)

var config struct {
	switchAddr string
	mqttAddr   string
	clientId   string
	topic      string
	username   string
	password   string
}

// Keep track of what this program believes is the current state
var currentState bool

func init() {
	flag.StringVar(&config.switchAddr, "swaddr", "127.0.0.1", "TP-Link plug/switch address")
	flag.StringVar(&config.mqttAddr, "mqttaddr", "127.0.0.1:1883", "MQTT host:port")
	flag.StringVar(&config.clientId, "clientid", "", "MQTT client ID")
	flag.StringVar(&config.topic, "topic", "tplink", "MQTT topic")
	flag.StringVar(&config.username, "user", "", "MQTT client username")
	flag.StringVar(&config.password, "pass", "", "MQTT client password")
	flag.Parse()

	if config.clientId == "" {
		config.clientId = fmt.Sprintf("tplink-%04d", rand.Intn(9999))
	}
}

func main() {
	// Set up channel on which to send signal notifications.
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, os.Kill)

	// Create connection to TP-Link switch
	tplink := hsplug.NewPlug(config.switchAddr)

	// Create an MQTT client
	cli := client.New(&client.Options{
		ErrorHandler: func(err error) {
			log.Fatalln("MQTT error:", err)
		},
	})
	defer cli.Terminate()

	// Connect to the MQTT server
	err := cli.Connect(&client.ConnectOptions{
		Network:  "tcp",
		Address:  config.mqttAddr,
		ClientID: []byte(config.clientId),
		UserName: []byte(config.username),
		Password: []byte(config.password),
	})
	if err != nil {
		log.Fatalln("Error connecting to MQTT server:", err)
	}

	// Send current swtich state on startup
	sendMqttState(cli, tplink.IsOn())

	// Channel used to trigger sending of status updates
	update := make(chan bool)

	// Set up a ticker to query and update switch status periodically - this
	// is needed to catch state changes made on the switch itself
	ticker := time.NewTicker(time.Second * 10)

	// Goroutine used to periodically and on demand send an updated MQTT state
	go func() {
		for {
			// Wait for either channel to be ready
			select {
			case _ = <-update:
			case _ = <-ticker.C:
			}

			newState := tplink.IsOn()
			if newState != currentState {
				sendMqttState(cli, newState)
			}
		}
	}()

	// Subscribe to topics.
	err = cli.Subscribe(&client.SubscribeOptions{
		SubReqs: []*client.SubReq{
			&client.SubReq{
				TopicFilter: []byte("cmnd/" + config.topic + "/power"),
				QoS:         mqtt.QoS1,
				Handler: func(topicName, message []byte) {
					state := string(message)
					log.Println("Received requested state:", state)
					if state == "on" {
						tplink.TurnOn()
					} else if state == "off" {
						tplink.TurnOff()
					}

					// Send updated switch status in background goroutine
					update <- true
				},
			},
		},
	})
	if err != nil {
		panic(err)
	}

	// Wait forever until signal is received
	<-sigc

	// Disconnect from MQTT
	if err := cli.Disconnect(); err != nil {
		panic(err)
	}
}

func sendMqttState(cli *client.Client, state bool) {
	var stateMsg string
	if state == true {
		stateMsg = "on"
	} else {
		stateMsg = "off"
	}

	err := cli.Publish(&client.PublishOptions{
		QoS:       mqtt.QoS1,
		Retain:    true,
		TopicName: []byte("stat/" + config.topic + "/power"),
		Message:   []byte(stateMsg),
	})
	if err != nil {
		log.Println("Error sending state update:", err)
	} else {
		currentState = state
		log.Println("New state published:", stateMsg)
	}
}
