# tplink-hsxxx-mqtt - TP-Link HSxxx to MQTT Bridge

This project is a simple app that can connect a TP-Link HSxxx (HS100, HS110, HS200) WiFi plug/switch to an MQTT broker. It is based on the work in the [HS1XXPlug](https://github.com/sausheong/hs1xxplug) project as well as the [GMQ project](https://github.com/yosssi/gmq).

So far it has been tested with the Mosquitto MQTT broker and an HS200 device, but HS1XXPlug should support the HS100 and HS110 as well.

At this time the program only supports a single HSxxx device, so multiple instances will need to be run in order to control multiple devices. This may change in the future.

## Download

Binaries are available on the Gitlab [tags](https://gitlab.com/davidf/tplink-hsxxx-mqtt/tags) page.

## Build/Install

Go (golang) and Git are required to build.

```
$ go get gitlab.com/davidf/tplink-hsxxx-mqtt
```

## Usage

```
Usage of tplink-hsxxx-mqtt:
  -clientid string
        MQTT client ID (default "tplink")
  -mqttaddr string
        MQTT host:port (default "127.0.0.1:1883")
  -pass string
        Password
  -swaddr string
        TP-Link switch address (default "127.0.0.1")
  -topic string
        MQTT topic (default "tplink")
  -user string
        Username
```

## Example: Running on Debian/Raspbian with supervisord

### Install supervisord (if not already installed)
```
sudo apt-get install supervisor
```

### Create /etc/supervisor/conf.d/tplink.conf
```
[program:famroom-light]
command=/home/davidf/go/bin/tplink-hsxxx-mqtt
	-swaddr 192.168.1.74
    -mqttaddr 192.168.1.99:1883
    -user client
    -pass secretpassword
    -topic familyroom
user=davidf
autostart=true
autorestart=true
```
### Tell supervisor to use the new config file and start the process
```
sudo supervisorctl reread
sudo supervisorctl add famroom-light
```